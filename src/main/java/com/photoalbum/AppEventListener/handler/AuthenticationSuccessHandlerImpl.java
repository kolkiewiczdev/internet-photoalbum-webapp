package com.photoalbum.AppEventListener.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

/**
 * @author KOlkiewicz
 */
@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        handler(request, response, authentication);

        HttpSession session = request.getSession();

        if(session != null) {
            session.setMaxInactiveInterval(30 * 60);
        }
        clearAuthenticationAttr(request);
    }

    protected void handler(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        String url = determineURLAddress(authentication);

        if(response.isCommitted()) {
            return;
        }

        redirectStrategy.sendRedirect(request, response, url);
    }

    protected String determineURLAddress(Authentication authentication) {
        boolean isAdmin = false;
        boolean isUser = false;

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        for(GrantedAuthority grantedAuthority: authorities) {
            if(grantedAuthority.getAuthority().equals("USER_PRIVILEGE")) {
                isUser = true;
            } else if(grantedAuthority.getAuthority().equals("ADMIN_PRIVILEGE")) {
                isAdmin = true;
                isUser = false;
                break;
            }
        }

        if(isUser) {
            return "/";
        }
        else if(isAdmin) {
            return "/admin/panel/";
        }
        else {
            throw new IllegalStateException();
        }
    }

    protected void clearAuthenticationAttr(HttpServletRequest request) {
        HttpSession session = request.getSession();

        if(session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    // getters, setters
    public RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
}
