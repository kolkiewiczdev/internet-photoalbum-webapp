package com.photoalbum.dao;

import com.photoalbum.dto.Gallery;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface GalleryRepository extends JpaRepository<Gallery, Long>{
    Gallery findById(Long id);
}
