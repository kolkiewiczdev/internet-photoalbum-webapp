package com.photoalbum.dao;

import com.photoalbum.dto.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
    PasswordResetToken findByToken(String token);
}
