package com.photoalbum.dao;

import com.photoalbum.dto.PhotoAlbumPage;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface PhotoAlbumPageRepository extends JpaRepository<PhotoAlbumPage, Long>{
}
