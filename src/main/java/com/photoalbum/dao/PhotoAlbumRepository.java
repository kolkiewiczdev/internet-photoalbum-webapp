package com.photoalbum.dao;

import com.photoalbum.dto.PhotoAlbum;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface PhotoAlbumRepository extends JpaRepository<PhotoAlbum, Long>{
}
