package com.photoalbum.dao;

import com.photoalbum.dto.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface PhotoRepository extends JpaRepository<Photo, Long>{
    Photo findById(Long photoId);
}
