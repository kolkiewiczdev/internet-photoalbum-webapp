package com.photoalbum.dao;

import com.photoalbum.dto.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
}
