package com.photoalbum.dao;

import com.photoalbum.dto.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String roleName);
}
