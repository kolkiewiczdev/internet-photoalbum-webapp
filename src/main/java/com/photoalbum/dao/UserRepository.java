package com.photoalbum.dao;

import com.photoalbum.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KOlkiewicz
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
