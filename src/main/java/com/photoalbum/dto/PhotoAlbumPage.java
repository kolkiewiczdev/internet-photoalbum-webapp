package com.photoalbum.dto;

import javax.persistence.*;
import java.util.Arrays;

/**
 * @author KOlkiewicz
 */
@Entity
@Table(name = "PhotoAlbumPage")
@AttributeOverride(name = "id", column = @Column(name = "PhotoAlbumPageID"))
/*@NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class PhotoAlbumPage extends BaseEntity{

        @Lob
        private byte[] file;

        private String mimeType;

        private String fileName;

        @Lob
        private String DataJSON;

        // getters, setters

        public byte[] getFile() {
                return file;
        }

        public void setFile(byte[] file) {
                this.file = file;
        }

        public String getMimeType() {
                return mimeType;
        }

        public void setMimeType(String mimeType) {
                this.mimeType = mimeType;
        }

        public String getFileName() {
                return fileName;
        }

        public void setFileName(String fileName) {
                this.fileName = fileName;
        }

        public String getDataJSON() {
                return DataJSON;
        }

        public void setDataJSON(String dataJSON) {
                DataJSON = dataJSON;
        }

        // hashCode, equals
        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                PhotoAlbumPage that = (PhotoAlbumPage) o;

                if (!Arrays.equals(file, that.file)) return false;
                if (mimeType != null ? !mimeType.equals(that.mimeType) : that.mimeType != null) return false;
                if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;
                return !(DataJSON != null ? !DataJSON.equals(that.DataJSON) : that.DataJSON != null);

        }

        @Override
        public int hashCode() {
                int result = file != null ? Arrays.hashCode(file) : 0;
                result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
                result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
                result = 31 * result + (DataJSON != null ? DataJSON.hashCode() : 0);
                return result;
        }
}
