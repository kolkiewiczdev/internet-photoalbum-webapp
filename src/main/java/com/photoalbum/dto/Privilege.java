package com.photoalbum.dto;

import javax.persistence.*;
import java.util.Collection;

/**
 * @author KOlkiewicz
 */
@Entity
@Table(name = "Privilege")
@AttributeOverride(name = "id", column = @Column(name = "PrivilegeID"))
/* @NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class Privilege extends BaseEntity {

    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;

    public Privilege() {
        super();
    }

    public Privilege(final String name) {
        super();
        this.name = name;
    }

    // getters, setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    // hashcode, equals methods

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Privilege privilege = (Privilege) o;

        if (name != null ? !name.equals(privilege.name) : privilege.name != null) return false;
        return !(roles != null ? !roles.equals(privilege.roles) : privilege.roles != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        return result;
    }

    // toString method

    /*
    @Override
    public String toString() {
        return "Privilege{" +
                "name='" + name + '\'' +
                ", roles=" + roles +
                '}';
    } */
}
