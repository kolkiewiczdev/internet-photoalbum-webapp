package com.photoalbum.dto.form;

import com.photoalbum.validation.PasswordMatches;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author KOlkiewicz
 */
@PasswordMatches
public class ChangePasswordFormDto {

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String password;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String matchingPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChangePasswordFormDto that = (ChangePasswordFormDto) o;

        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        return !(matchingPassword != null ? !matchingPassword.equals(that.matchingPassword) : that.matchingPassword != null);

    }

    @Override
    public int hashCode() {
        int result = password != null ? password.hashCode() : 0;
        result = 31 * result + (matchingPassword != null ? matchingPassword.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChangePasswordFormDto{" +
                "password='" + password + '\'' +
                ", matchingPassword='" + matchingPassword + '\'' +
                '}';
    }
}
