package com.photoalbum.service;

import com.photoalbum.dao.*;
import com.photoalbum.dto.*;
import com.photoalbum.dto.form.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Blob;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author KOlkiewicz
 */
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    GalleryRepository galleryRepository;

    @Autowired
    PhotoRepository photoRepository;

    @Autowired
    PhotoAlbumRepository photoAlbumRepository;

    @Autowired
    PhotoAlbumPageRepository photoAlbumPageRepository;

    @Autowired
    VerificationTokenRepository tokenRepository;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public User createUser(UserDTO userDTO) {

        if(isUserExists(userDTO.getEmail()))
            return null;

        User user = new User();

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));

        user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));

        return userRepository.save(user);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUser(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isUserExists(String email){
        User user = userRepository.findByEmail(email);

        if(user != null)
            return true;

        return false;
    }

    @Override
    public void createUserVerificationToken(User user, String token) {
        VerificationToken verificationToken = new VerificationToken(token, user);
        tokenRepository.save(verificationToken);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        VerificationToken findToken = tokenRepository.findByToken(token);
        return findToken;
    }

    @Override
    public void createPasswordResetToken(User user, String token) {
        PasswordResetToken passwordResetToken = new PasswordResetToken(token, user);
        passwordResetTokenRepository.save(passwordResetToken);
    }

    @Override
    public PasswordResetToken getPasswordResetToken(String token) {
        return passwordResetTokenRepository.findByToken(token);
    }

    @Override
    public void updateUserPassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public void updateUserVerificationToken(User user, String token) {
        VerificationToken verificationToken = tokenRepository.findByUser_id(user.getId());
        verificationToken.setToken(token);
        verificationToken.updateExpiryDate();
        tokenRepository.save(verificationToken);
    }

    @Override
    public Collection<Gallery> getUserGalleries(String email) {
        User user = getUser(email);
        return user.getGalleries();
    }

    @Override
    public Gallery setNewUserGallery(String email, String galleryName) {
        User user = getUser(email);

        Gallery gallery = new Gallery();
        gallery.setGalleryName(galleryName);
        Gallery gallery1 = galleryRepository.save(gallery);

        user.addGallery(gallery);

        userRepository.save(user);

        return gallery1;
    }

    @Override
    public Collection<Photo> getGalleryPhotos(Long GalleryId) {
        Gallery gallery = galleryRepository.findById(GalleryId);

        Collection<Photo> photos = gallery.getImages();

        return photos;
    }

    @Override
    public void addNewPhoto(Long GalleryId, Photo photo) {
        Gallery gallery = galleryRepository.findById(GalleryId);

        gallery.addPhoto(photo);

        galleryRepository.save(gallery);
    }

    @Override
    public void deleteGallery(String userName, Long id) {
        User user = userRepository.findByEmail(userName);

        Gallery gallery = galleryRepository.findById(id);

        List<Gallery> list = (List<Gallery>) user.getGalleries();
        list.remove(gallery);

        user.setGalleries(list);
        userRepository.save(user);

        galleryRepository.delete(gallery);
    }

    @Override
    public void deletePhoto(String userName, Long galleryId, Long id) {
        Gallery gallery = galleryRepository.findById(galleryId);
        Photo photo = photoRepository.findById(id);

        List<Photo> list = (List<Photo>) gallery.getImages();
        list.remove(photo);

        gallery.setImages(list);
        galleryRepository.save(gallery);
    }

    @Override
    public Collection<PhotoAlbum> getUserPhotoAlbums(String email) {
        User user = getUser(email);

        return user.getPhotoAlbums();
    }

    @Override
    public PhotoAlbum setNewUserPhotoAlbum(String email, String photoAlbumName) {
        User user = getUser(email);

        PhotoAlbum photoAlbum = new PhotoAlbum();
        photoAlbum.setPhotoAlbumName(photoAlbumName);

        PhotoAlbum photoAlbum1 = photoAlbumRepository.save(photoAlbum);

        user.addPhotoAlbum(photoAlbum);
        userRepository.save(user);

        return photoAlbum1;
    }

    @Override
    public PhotoAlbum getPhotoAlbum(Long id) {
        return photoAlbumRepository.findOne(id);
    }

    @Override
    public void SaveAlbumPhotoPage(Long albumID, int photoPageID, String JSONPage, String CanvasToJPEG) {
        PhotoAlbum photoAlbum = photoAlbumRepository.findOne(albumID);
        List<PhotoAlbumPage> photoAlbumPages = (List<PhotoAlbumPage>) photoAlbum.getPhotoAlbumPages();

        if(photoAlbumPages.size() <= photoPageID){
            PhotoAlbumPage photoAlbumPage = new PhotoAlbumPage();
            photoAlbumPage.setDataJSON(JSONPage);
            photoAlbumPage.setFile(CanvasToJPEG.getBytes());
            photoAlbumPages.add(photoAlbumPage);
        } else {
            PhotoAlbumPage photoAlbumPage = photoAlbumPages.get(photoPageID);
            photoAlbumPage.setDataJSON(JSONPage);
            photoAlbumPage.setFile(CanvasToJPEG.getBytes());
            photoAlbumPages.set(photoPageID, photoAlbumPage);
        }

        //photoAlbumPages.add(photoPageID, photoAlbumPage);
        photoAlbum.setPhotoAlbumPages(photoAlbumPages);

        photoAlbumRepository.save(photoAlbum);
    }

    @Override
    public void deletePhotoAlbum(String userName, Long id) {
        User user = userRepository.findByEmail(userName);

        PhotoAlbum photoAlbum = photoAlbumRepository.findOne(id);

        List<PhotoAlbum> photoAlbums = (List<PhotoAlbum>) user.getPhotoAlbums();
        photoAlbums.remove(photoAlbum);

        user.setPhotoAlbums(photoAlbums);
        userRepository.save(user);

        photoAlbumRepository.delete(photoAlbum);
    }

    @Override
    public List<PhotoAlbumPage> photoAlbumPages(Long id) {
        PhotoAlbum photoAlbum = photoAlbumRepository.findOne(id);

        return (List<PhotoAlbumPage>) photoAlbum.getPhotoAlbumPages();
    }

    @Override
    public void deletePhotoAlbumPage(Long albumID, Long id) {
        PhotoAlbum photoAlbum = photoAlbumRepository.findOne(albumID);
        PhotoAlbumPage photoAlbumPage = photoAlbumPageRepository.findOne(id);

        photoAlbum.getPhotoAlbumPages().remove(photoAlbumPage);

        photoAlbumRepository.save(photoAlbum);
    }
}
