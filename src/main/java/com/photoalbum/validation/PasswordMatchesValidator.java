package com.photoalbum.validation;

import com.photoalbum.dto.form.ChangePasswordFormDto;
import com.photoalbum.dto.form.UserDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author KOlkiewicz
 */
public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object>{

    @Override
    public void initialize(PasswordMatches passwordMatches) {
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if(o instanceof UserDTO) {
            UserDTO userDTO = (UserDTO) o;
            return userDTO.getPassword().equals(userDTO.getMatchingPassword());
        }
        if(o instanceof ChangePasswordFormDto){
            ChangePasswordFormDto changePasswordFormDto = (ChangePasswordFormDto) o;
            return changePasswordFormDto.getPassword().equals(changePasswordFormDto.getMatchingPassword());
        }
        return false;
    }
}
