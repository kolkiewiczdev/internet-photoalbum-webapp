package com.photoalbum.web;

import com.photoalbum.service.config.ServiceConfiguration;
import com.photoalbum.web.config.PersistenceJPAConfig;
import com.photoalbum.web.config.SecurityConfiguration;
import com.photoalbum.web.config.WebConfiguration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * @author KOlkiewicz
 */
@Order(1)
public class PhotoAlbumApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {PersistenceJPAConfig.class, SecurityConfiguration.class, ServiceConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {WebConfiguration.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
