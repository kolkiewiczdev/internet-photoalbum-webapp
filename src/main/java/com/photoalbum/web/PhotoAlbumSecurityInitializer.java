package com.photoalbum.web;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author KOlkiewicz
 */
@Order(2)
public class PhotoAlbumSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
