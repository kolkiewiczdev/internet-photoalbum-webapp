package com.photoalbum.web.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * @author KOlkiewicz
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationSuccessHandler AuthenticationSuccessHandlerImpl;

    // Beans
    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder(12);
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        return daoAuthenticationProvider;
    }

    // Override methods
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
                .csrf().disable()
                .authorizeRequests()
                    .expressionHandler(new DefaultWebSecurityExpressionHandler())
                .antMatchers("/j_spring_security_check*", "/", "/resources/**", "/webjars/**", "/assets/**", "/JSMessages/**",
                        "/user/**/**")
                    .permitAll() //
                //.antMatchers("/invalidSession*").anonymous()
                .antMatchers("/user/register/").anonymous()
                .antMatchers("/user/controlPanel/**").authenticated()
                .anyRequest().authenticated()
                .and()
                    .formLogin()
                        .loginPage("/user/login/")
                        .loginProcessingUrl("/user/login/j_spring_security_check")
                        .defaultSuccessUrl("/") //homepage
                        .failureUrl("/user/login/?error=true")
                        .successHandler(AuthenticationSuccessHandlerImpl)
                        .usernameParameter("j_username")
                        .passwordParameter("j_password")
                .permitAll()
                    .and()
                .sessionManagement()
                    //.invalidSessionUrl("/invalidSession/")
                    .sessionFixation().none()
                .and()
                .logout()
                        .invalidateHttpSession(false)
                        .logoutUrl("/user/logout/j_spring_security_logout")
                        .logoutSuccessUrl("/")
                        .deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .anonymous()
                .principal("guest")
                .authorities("ROLE_GUEST")
                .and()
                .rememberMe()
        ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.authenticationProvider(authenticationProvider());
    }


    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }
}
