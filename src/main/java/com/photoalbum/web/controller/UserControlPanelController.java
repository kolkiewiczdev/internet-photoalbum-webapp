package com.photoalbum.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.photoalbum.dto.Gallery;
import com.photoalbum.dto.Photo;
import com.photoalbum.dto.PhotoAlbum;
import com.photoalbum.dto.PhotoAlbumPage;
import com.photoalbum.service.UserService;
import com.photoalbum.web.util.GenericResponse;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;

import org.springframework.security.core.Authentication;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author KOlkiewicz
 */
@Controller
@RequestMapping("/user/controlPanel/")
public class UserControlPanelController {
    private final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String showUserControlPanel(Model model, Authentication authentication){
        Collection<Gallery> userGalleries = userService.getUserGalleries(authentication.getName());

        model.addAttribute("userGalleries", userGalleries);

        return "userControlPanel";
    }

    @RequestMapping(value = "PhotoAlbum/", method = RequestMethod.GET)
    public String showPhotoAlbum(Model model, Authentication authentication){
        Collection<PhotoAlbum> photoAlbums = userService.getUserPhotoAlbums(authentication.getName());

        model.addAttribute("photoAlbums", photoAlbums);
        return "PhotoAlbums";
    }

    @RequestMapping(value = "PhotoAlbum/delete/{id}", method = RequestMethod.GET)
    public String deletePhotoAlbum(Authentication authentication, @PathVariable Long id){
        String userName = authentication.getName();

        userService.deletePhotoAlbum(userName, id);
        return "redirect:/user/controlPanel/PhotoAlbum/";
    }

    @RequestMapping(value = "PhotoAlbum/preview/{id}", method = RequestMethod.GET)
    public String previewPhotoAlbum(Model model, @PathVariable Long id){
        List<PhotoAlbumPage> photoAlbumPages = userService.photoAlbumPages(id);

        model.addAttribute("albumID", id);
        model.addAttribute("photos", photoAlbumPages);
        return "PhotoAlbumPreview";
    }

    @RequestMapping(value = "PhotoAlbum/preview/deletePhoto/{albumID}/{id}", method = RequestMethod.GET)
    public String deletePhotoAlbumPage(Model model, @PathVariable Long albumID, @PathVariable Long id){
        userService.deletePhotoAlbumPage(albumID, id);

        return "redirect:/user/controlPanel/PhotoAlbum/preview/" + albumID;
    }

    @RequestMapping(value = "PhotoAlbum/{id}/{page}/", method = RequestMethod.GET)
    public String showPhotoAlbumWizard(@PathVariable long id, @PathVariable int page, Model model, Authentication authentication){
        PhotoAlbum photoAlbum = userService.getPhotoAlbum(id);

        if(photoAlbum.getPhotoAlbumPages().size() >= page) {
            List<PhotoAlbumPage> list = (List<PhotoAlbumPage>) photoAlbum.getPhotoAlbumPages();

            model.addAttribute("photoPageJSON", list.get(page-1).getDataJSON());
        }

        model.addAttribute("albumID", id);
        model.addAttribute("photoPage", page);

        Collection<Gallery> userGalleries = userService.getUserGalleries(authentication.getName());
        model.addAttribute("userGalleries", userGalleries);

        return "PhotoAlbumWizard";
    }

    @RequestMapping(value = "Gallery/{id}", method = RequestMethod.GET)
    public String showGallery(Model model, @PathVariable Long id){
        Collection<Photo> photos = userService.getGalleryPhotos(id);

        model.addAttribute("photos", photos);

        return "Gallery";
    }

    @RequestMapping(value = "Gallery/{id}/", method = RequestMethod.POST)
    public GenericResponse uploadPhotos(MultipartHttpServletRequest request, @PathVariable Long id){
        try {
            Iterator<String> iterator = request.getFileNames();

        while(iterator.hasNext()){
            String uploadedFile = iterator.next();

            MultipartFile file = request.getFile(uploadedFile);

            String fileName = file.getOriginalFilename();
            String mimeType = file.getContentType();
            byte[] bytes = file.getBytes();

            Photo photo = new Photo(fileName, mimeType, bytes);

            userService.addNewPhoto(id, photo);
            }
        } catch (IOException e) {
                e.printStackTrace();
        }

        return new GenericResponse("success");
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public String deleteGallery(Authentication authentication, @PathVariable Long id){
        String userName = authentication.getName();

        userService.deleteGallery(userName, id);
        return "redirect:/user/controlPanel/";
    }

    @RequestMapping(value = "Gallery/{galleryId}/deletePhoto/{id}", method = RequestMethod.GET)
    public String deletePhoto(Authentication authentication, @PathVariable Long galleryId, @PathVariable Long id){
        String userName = authentication.getName();

        userService.deletePhoto(userName, galleryId, id);
        return "redirect:/user/controlPanel/Gallery/" + galleryId + "/";
    }

    // Add new Gallery, Ajax common function
    @RequestMapping(value = "addGallery", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    GenericResponse addNewGallery(@RequestBody String galleryName, HttpServletRequest request, Authentication authentication) throws IOException {

        ObjectMapper m = new ObjectMapper();
        JsonNode jsonNode = m.readTree(galleryName);
        String content = jsonNode.path("galleryName").asText();

        Gallery gallery = userService.setNewUserGallery(authentication.getName(), content);

        return new GenericResponse(gallery.getId() + "");
    }

    // Add new PhotoAlbum, Ajax common function
    @RequestMapping(value = "addPhotoAlbum", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    GenericResponse addNewPhotoAlbum(@RequestBody String photoAlbumName, HttpServletRequest request, Authentication authentication) throws IOException {

        ObjectMapper m = new ObjectMapper();
        JsonNode jsonNode = m.readTree(photoAlbumName);
        String content = jsonNode.path("photoAlbumName").asText();

        PhotoAlbum photoAlbum = userService.setNewUserPhotoAlbum(authentication.getName(), content);

        return new GenericResponse(photoAlbum.getId() + "");
    }

    // Add new PhotoAlbumPage, Ajax common function
    @RequestMapping(value = "savePhotoPage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    GenericResponse savePhotoPage(@RequestBody String data, HttpServletRequest request, Authentication authentication) throws IOException {

        ObjectMapper m = new ObjectMapper();

        JsonNode jsonNode = m.readTree(data);
        String content = jsonNode.path("canvas").asText();

        JsonNode jsonNode2 = m.readTree(data);
        Long albumID = jsonNode.path("albumID").asLong();

        JsonNode jsonNode3 = m.readTree(data);
        Integer photoAlbumPageID = jsonNode.path("photoAlbumPageID").asInt();

        JsonNode jsonNode4 = m.readTree(data);
        String canvasToJPEG = jsonNode.path("canvasToJPEG").asText();

        userService.SaveAlbumPhotoPage(albumID, photoAlbumPageID-1, content, canvasToJPEG);

        return new GenericResponse("success");
    }
}
