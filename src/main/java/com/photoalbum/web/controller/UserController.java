package com.photoalbum.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.photoalbum.AppEventListener.event.OnRegistrationSuccessEvent;
import com.photoalbum.dto.PasswordResetToken;
import com.photoalbum.dto.User;
import com.photoalbum.dto.VerificationToken;
import com.photoalbum.dto.form.ChangePasswordFormDto;
import com.photoalbum.dto.form.UserDTO;
import com.photoalbum.service.UserService;
import com.photoalbum.web.util.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

/**
 * @author KOlkiewicz
 */
@Controller
@RequestMapping("/user/")
public class UserController {

    private final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    @Autowired
    ApplicationEventPublisher eventPublisher;

    @Autowired
    MessageSource messageSource;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    UserDetailsService userDetailsService;

    // LOGIN

    @RequestMapping(value = "login/", method = RequestMethod.GET)
    public String showLoginPage() {
        return "login";
    }

    // END LOGIN

    // REGISTRATION

    // Register new Account, GET, show Form
    @RequestMapping(value = "register/", method = RequestMethod.GET)
    public String showRegisterPage(WebRequest request) {
        return "register";
    }

    // Register new Account, POST
    @RequestMapping(value = "register/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GenericResponse registerUserAccount(@RequestBody @Valid UserDTO userDTO, HttpServletRequest request, Locale locale)
    {
        LOGGER.debug("Registration of account: ", userDTO);

        User user = userService.createUser(userDTO);

        if(user == null) {
            return new GenericResponse("failed");
        }

        String URL = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        eventPublisher.publishEvent(new OnRegistrationSuccessEvent(URL, locale, user));

        return new GenericResponse("success");
    }

    // Activate account by sent token, GET
    @RequestMapping(value = "confirmRegistration", method = RequestMethod.GET)
    public String activateAccount(@RequestParam("token") String token, Model model, Locale locale) {
        VerificationToken verifToken = userService.getVerificationToken(token);

        if(verifToken == null){
            model.addAttribute("message", messageSource.getMessage("InvalidToken.Invalid", null, locale));
            return "redirect:/user/invalidToken/?language=" + locale.getLanguage();
        }

        User user = verifToken.getUser();
        Calendar calendar = Calendar.getInstance();

        if( ( (verifToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) ){
            model.addAttribute("message", messageSource.getMessage("InvalidToken.Expired", null, locale));
            return "redirect:/user/invalidToken/?language=" + locale.getLanguage();
        }

        // if token is valid

        user.setEnabled(true);
        userService.saveUser(user);
        // set messages to Model
        return "redirect:login/";
    }

    // Show Reset Password Form, GET
    @RequestMapping(value = "resetPassword/", method = RequestMethod.GET)
    public String showResetPasswordForm(HttpServletRequest request, Model model) {
        return "resetPassword";
    }

    // Send reset password token, POST
    @RequestMapping(value = "resetPassword/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GenericResponse sendResetPasswordEmail(@RequestBody String email, HttpServletRequest request, Locale locale) throws IOException {

        ObjectMapper m = new ObjectMapper();
        JsonNode jsonNode = m.readTree(email);
        String content = jsonNode.path("email").asText();

        User user = userService.getUser(content);

        if(user == null){
            return new GenericResponse("failed");
        }

        String token = UUID.randomUUID().toString();
        userService.createPasswordResetToken(user, token);

        // make and send email for user...
        String URL = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        SimpleMailMessage mailMessage = createPasswordResetTokenEmail(URL, user, token, locale);
        javaMailSender.send(mailMessage);

        return new GenericResponse("success");
    }

    // Change password, GET
    @RequestMapping(value = "changePassword/", method = RequestMethod.GET)
    public String showChangePasswordForm(Model model, Locale locale, @RequestParam("id") long userID, @RequestParam("token") String token){
        PasswordResetToken passwordResetToken = userService.getPasswordResetToken(token);
        User user = passwordResetToken.getUser();

        if((user.getId() != userID) && passwordResetToken == null) {
            return "redirect:/user/login/";
        }

        Calendar calendar = Calendar.getInstance();

        if((passwordResetToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
            return "redirect:/user/login/";
        }

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, userDetailsService.loadUserByUsername(user.getEmail()).getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return "redirect:/user/updatePass/";
    }

    // Show update pass form, GET
    @RequestMapping(value = "updatePass/", method = RequestMethod.GET)
    public String showUpdatePassForm(){
        return "updatePass";
    }

    // Update pass form, POST
    @RequestMapping(value = "updatePass/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GenericResponse updatePassword(@RequestBody @Valid ChangePasswordFormDto changePasswordFormDto, HttpServletRequest request)
    {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.updateUserPassword(user, changePasswordFormDto.getPassword());

        SecurityContextHolder.clearContext();

        return new GenericResponse("success");
    }

    // Resend activation token, GET
    @RequestMapping(value = "resendActivationToken/", method = RequestMethod.GET)
    public String resendActivationToken() {
        return "resendActivationToken";
    }

    // Send reset password token, POST
    @RequestMapping(value = "resendActivationToken/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GenericResponse resendActivationTokenEmail(@RequestBody String email, HttpServletRequest request, Locale locale) throws IOException {

        ObjectMapper m = new ObjectMapper();
        JsonNode jsonNode = m.readTree(email);
        String content = jsonNode.path("email").asText();

        User user = userService.getUser(content);

        if(user == null){
            return new GenericResponse("failed");
        }

        String token = UUID.randomUUID().toString();

        userService.updateUserVerificationToken(user, token);

        // make and send email for user...
        String URL = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        SimpleMailMessage mailMessage = createResendActivationTokenEmail(URL, user, token, locale);
        javaMailSender.send(mailMessage);

        return new GenericResponse("success");
    }

    //TODO Napisac jakas ladna stron�, poprawic ten syf.
    // Invalid token page, GET
    @RequestMapping(value = "invalidToken/", method = RequestMethod.GET)
    public String showInvalidTokenErrPage(Model model, HttpServletRequest request){
        model.addAttribute("message", request.getParameter("message"));
        return "invalidToken";
    }

    // Validate Email Address, Ajax common function
    @RequestMapping(value = "register/validateEmail/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GenericResponse validateEmail(@RequestBody String email, HttpServletRequest request) throws IOException {

        ObjectMapper m = new ObjectMapper();
        JsonNode jsonNode = m.readTree(email);
        String content = jsonNode.path("email").asText();

        boolean exists = userService.isUserExists(content);

        if(exists) {
            return new GenericResponse("failed");
        }

        return new GenericResponse("success");
    }

    // Check Email Exists in DB, Ajax common function
    @RequestMapping(value = "register/checkIfExistsEmail/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GenericResponse checkIfExistsEmail(@RequestBody String email, HttpServletRequest request) throws IOException {

        ObjectMapper m = new ObjectMapper();
        JsonNode jsonNode = m.readTree(email);
        String content = jsonNode.path("email").asText();

        boolean exists = userService.isUserExists(content);

        if(exists) {
            return new GenericResponse("success");
        }

        return new GenericResponse("failed");
    }

    // END REGISTRATION

    // NON API METHODS

    private SimpleMailMessage createPasswordResetTokenEmail(String URL, User user, String token, Locale locale){
        String url = URL + "/user/changePassword/?id=" + user.getId() + "&token=" + token;
        String message = messageSource.getMessage("PasswordResetTokenEmail.Message", new Object[]{url}, locale);
        String subject = messageSource.getMessage("PasswordResetTokenEmail.Subject", null, locale);

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(user.getEmail());
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(message);
        simpleMailMessage.setFrom("support@photoalbum.com");
        return simpleMailMessage;
    }

    private SimpleMailMessage createResendActivationTokenEmail(String URL, User user, String token, Locale locale) {
        String sentToAddress = user.getEmail();
        String subject = messageSource.getMessage("RegisterConfirmEmail.Subject", null, locale);
        String emailMessage = messageSource.getMessage("RegisterConfirmEmail.Message", null, locale);
        String confirmURL = URL + "/user/confirmRegistration?token=" + token;

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(sentToAddress);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(emailMessage + " \r\n" + confirmURL);
        simpleMailMessage.setFrom("support@photoalbum.com");
        return simpleMailMessage;
    }
}
