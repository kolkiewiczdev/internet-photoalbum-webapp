<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jstl" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="GalleryPage.SiteTitle" var="pageTitle"/>

<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />
  <link href="${DropZoneCSS}" rel="stylesheet" />
  <link href="${FancyBoxCSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
  <script src="${jQueryUI}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

  <script src="${DropZone}"></script>
  <script src="${FancyBoxJS}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <script>
    $(document).ready(function() {
      $(".fancybox").fancybox({
        type: "image",
        openEffect	: 'none',
        closeEffect	: 'none'
      });

      $('#followMenu').hide();

      // fix menu when passed
      $('#MainHeader')
              .visibility({
                once: false,
                onBottomPassed: function () {
                  $('#followMenu').transition('fade in');
                },
                onBottomPassedReverse: function () {
                  $('#followMenu').transition('fade out');
                }
              })
    });
  </script>
  <style>
    .ui.grid {
      margin-top: 20px;
      margin-bottom: 20px;
    }
    .floated.button {
      margin-right: 20px;
    }
    #wrapper {
      min-height: 100%;
    }
  </style>
</head>

<body>

<div id="wrapper">
<!-- Import Header-->
<%@ include file="header.jspf" %>


<div class="ui grid">
  <div class="four wide column">
    <div class="ui vertical fluid tabular menu">
      <a href="<c:url value="/user/controlPanel/"/>" class="item active">
        <spring:message code="UserControlPanelPage.TabGallery"/>
      </a>
      <a href="<c:url value="/user/controlPanel/PhotoAlbum/"/>" class="item">
        <spring:message code="UserControlPanelPage.TabPhotoAlbum"/>
      </a>
    </div>
  </div>
  <div class="twelve wide stretched column">
    <div class="ui clearing segment">
      <spring:message code="UserControlPanelPage.GalleryMessage"/>
    </div>

    <div class="ui three column grid" id="galleryGrid">

      <jstl:forEach items="${photos}" var="photo">
        <div class="column">
          <div class="ui image segment">
            <div class="image">
              <a class="fancybox" rel="gallery1" href="<c:url value="/showImage/photoalbumpage?id=${photo.getId()}"/>" title="${photo.getFileName()}">
                <img src="<c:url value="/showImage/photoalbumpage?id=${photo.getId()}"/>" alt="${photo.getFileName()}" />
              </a>
            </div>
          </div>
          <a href="<c:url value="deletePhoto/${albumID}/${photo.getId()}"/>">
            <button class="negative ui button">
              <spring:message code="Common.Delete"/>
            </button>
          </a>
        </div>
      </jstl:forEach>

    </div>
  </div>
</div>
</div>

<!-- Import Footer-->
<%@ include file="footer.jspf" %>

</body>
</html>