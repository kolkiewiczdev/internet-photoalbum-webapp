<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="RegisterPage.SiteTitle" var="pageTitle"/>


<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />

    <script src="${jQuery}"></script>
    <script src="${jQuerySerialize}"></script>
    <script src="${i18props}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

    <style>
        /* Form css */
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
    </style>

  <script>
      // get system locale for i18n
      function getSystemLocale() {
          var systemLocale ='<%=RequestContextUtils.getLocale(request)%>';
          return systemLocale;
      }

    $(document).ready(function() {

        // init i18n.properties plugin
        $.i18n.properties({
            name:'JS_Messages',
            path: '${JSMessages}/',
            mode:'both',
            language:getSystemLocale(),
            encoding: 'Windows-1250',
            callback: function() {
                console.log("Configured")
            }
        })

        // custom ui.form validation rule
        $.fn.form.settings.rules.EmailUnique = function(value) {
            var isSuccess = false;
            $.ajax({
                url: '<c:url value="/user/register/validateEmail/"/>',
                type: "post",
                data: JSON.stringify({
                    "email": value,
                }),
                dataType : "json",
                contentType: 'application/json; charset=utf-8',
                async: false,
                success : function(data){
                    isSuccess = data.message === "success";
                }
            }, value);

            return isSuccess; //
        };

        // init .ui.form validation
        $('.ui.form')
                    .form({
                        on: 'blur',
                        fields: {
                            firstName: {
                                identifier: 'firstName',
                                rules: [
                                    {
                                        type: 'empty',
                                        prompt: jQuery.i18n.prop('RegistrationForm.EmptyFirstName')
                                    },
                                    {
                                        type: 'maxLength[50]',
                                        prompt: jQuery.i18n.prop('RegistrationForm.MaxLengthFirstName')
                                    }
                                ]
                            },
                            lastName: {
                                identifier: 'lastName',
                                rules: [
                                    {
                                        type: 'empty',
                                        prompt: jQuery.i18n.prop('RegistrationForm.EmptyLastName')
                                    },
                                    {
                                        type: 'maxLength[50]',
                                        prompt: jQuery.i18n.prop('RegistrationForm.MaxLengthLastName')
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type: 'email',
                                        prompt: jQuery.i18n.prop('RegistrationForm.InvalidEmail')
                                    },
                                    {
                                        type: 'EmailUnique[email]',
                                        prompt: jQuery.i18n.prop('RegistrationForm.EmailUnique')
                                    }
                                ]
                            },
                            password: {
                                identifier: 'password',
                                rules: [
                                    {
                                        type: 'regExp[/((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/]',
                                        prompt: jQuery.i18n.prop('RegistrationForm.InvalidPassword')
                                    }
                                ]
                            },
                            matchingPassword: {
                                identifier: 'matchingPassword',
                                rules: [
                                    {
                                        type: 'match[password]',
                                        prompt: jQuery.i18n.prop('RegistrationForm.ConfirmPasswordMatchError')
                                    }
                                ]
                            }
                        }
                    })
            ;

        // send form by Ajax
       $('.ui.form').on('submit', function(e){
           e.preventDefault();
            $.ajax({
                url: '<c:url value="/user/register/"/>',
                type: "post",
                data: formToJSON(),
                dataType : "json",
                contentType: 'application/json; charset=utf-8',
                async: false,
                success : function(data){
                    if(data.message == "success") {
                        location.replace("<c:url value="/"/>");
                    }
                },
                error : function(xhr, status){
                    console.log(status);
                }
            })
        })
    })
;
      // pack ui.form data to JSON format
      function formToJSON() {
          return JSON.stringify({
              "firstName": $('.ui.form').form('get value', 'firstName'),
              "lastName": $('.ui.form').form('get value', 'lastName'),
              "email": $('.ui.form').form('get value', 'email'),
              "password": $('.ui.form').form('get value', 'password'),
              "matchingPassword": $('.ui.form').form('get value', 'matchingPassword')
          })
      }
  </script>
</head>

<body>

<div class="ui middle aligned center aligned grid">

  <div class="column">

    <h2 class="ui teal image header">

      <div class="content">
        <spring:message code="RegisterPage.HeaderText"/>
      </div>
    </h2>

    <form class="ui large form">
      <div class="ui stacked segment">

        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="firstName" placeholder='<spring:message code="RegisterPage.FirstName"/>' >
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="lastName" placeholder='<spring:message code="RegisterPage.LastName"/>' >
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <i class="mail outline icon"></i>
            <input type="text" name="email" placeholder='<spring:message code="RegisterPage.Email"/>' >
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder='<spring:message code="RegisterPage.Password"/>' >
          </div>
        </div>


        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="matchingPassword" placeholder='<spring:message code="RegisterPage.ConfirmPassword"/>' >
          </div>
        </div>

        <div class="ui fluid large teal submit button"><spring:message code="RegisterPage.RegisterButton" /> </div>

      </div>

      <div class="ui error message"></div>

    </form>

    <div class="ui info message">
      <spring:message code="RegisterPage.LoginPageRedirect"/> <a href="<c:url value="/user/login/"/>"><spring:message code="LoginPage.LoginButton"/></a>
    </div>

  </div>

</div>
</body>
</html>