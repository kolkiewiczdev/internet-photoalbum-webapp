<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="ResendActivationTokenPage.SiteTitle" var="pageTitle"/>


<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <style>
    /* Form css */
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>


  <script>
    // get system locale for i18n
    function getSystemLocale() {
      var systemLocale ='<%=RequestContextUtils.getLocale(request)%>';
      return systemLocale;
    }

    $(document).ready(function() {

      // init i18n.properties plugin
      $.i18n.properties({
        name:'JS_Messages',
        path: '${JSMessages}/',
        mode:'both',
        language:getSystemLocale(),
        encoding: 'Windows-1250',
        callback: function() {
          console.log("Configured")
        }
      });

      // custom ui.form validation rule
      $.fn.form.settings.rules.EmailExists = function(value) {
        var isSuccess = false;
        $.ajax({
          url: '<c:url value="/user/register/checkIfExistsEmail/"/>',
          type: "post",
          data: JSON.stringify({
            "email": value,
          }),
          dataType : "json",
          contentType: 'application/json; charset=utf-8',
          async: false,
          success : function(data){
            isSuccess = data.message === "success";
          }
        }, value);

        return isSuccess; //
      };

      // init .ui.form validation
      $('.ui.form')
              .form({
                on: 'blur',
                fields: {
                  email: {
                    identifier: 'email',
                    rules: [
                      {
                        type: 'email',
                        prompt: jQuery.i18n.prop('ResetPasswordForm.InvalidEmail')
                      },
                      {
                        type: 'EmailExists[email]',
                        prompt: jQuery.i18n.prop('ResetPasswordForm.EmailNotExists')
                      }
                    ]
                  }
                }
              })
      ;

      // send form by Ajax
      $('.ui.form').on('submit', function(e){
        e.preventDefault();
        $.ajax({
          url: '<c:url value="/user/resendActivationToken/"/>',
          type: "post",
          data: formToJSON(),
          dataType : "json",
          contentType: 'application/json; charset=utf-8',
          async: false,
          success : function(data){
            if(data.message == "success") {
              location.replace("<c:url value="/"/>");
            }
          },
          error : function(xhr, status){
            console.log(status);
          }
        })
      })
    });

    // pack ui.form data to JSON format
    function formToJSON() {
      return JSON.stringify({
        "email": $('.ui.form').form('get value', 'email'),
      })
    }
  </script>
</head>

<body>

<div class="ui middle aligned center aligned grid">

  <div class="column">

    <h2 class="ui teal image header">

      <div class="content">
        <spring:message code="ResendActivationTokenPage.HeaderText"/>
      </div>
    </h2>

    <form class="ui large form">
      <div class="ui stacked segment">

        <div class="field">
          <div class="ui left icon input">
            <i class="mail outline icon"></i>
            <input type="text" name="email" placeholder='<spring:message code="LoginPage.Email"/>' >
          </div>
        </div>

        <div class="ui fluid large teal submit button"><spring:message code="ResendActivationTokenPage.ResetButton" /> </div>

      </div>

      <div class="ui error message"></div>

    </form>
  </div>
</div>
</body>
</html>